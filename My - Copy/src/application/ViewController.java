package application;



import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;



import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.VBox;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;

public class ViewController  implements Initializable  {
	
	File[] listFile ;
	int i = 0;
	File file1;
	int lengthListFile;
	String number = ""; // tao bien String de chua id cua button 
	int numberInt = 0;  // bien chua id cua button kieu int
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
	}
	@FXML
	private MediaPlayer mediaPlayer;
	@FXML
	private MediaView mediaShow;
	private String filePath;
	@FXML
	private Button Library;
	@FXML
	private Button Stop;
	@FXML
	private Button Prev;
	@FXML
	private Button Play;
	@FXML
	private Button Next;
	@FXML
	private Button repeat;
	@FXML
	private Button Folder;
	@FXML
	private Button Songs;
	@FXML
	private ScrollPane scrollPane;
	
	@FXML
	private void handlerButtonAction(ActionEvent event) {
		FileChooser fileChooser = new FileChooser();
		FileChooser.ExtensionFilter filter= new FileChooser.ExtensionFilter("select file(*.mp3);(*.mp4);(*.wav);(*.wma)", "*.mp3","*.mp4","*.wav","*.wma" );
			fileChooser.getExtensionFilters().add(filter);
			File file = fileChooser.showOpenDialog(null);
			filePath = file.toURI().toString();
			if(filePath != null) {
				Media media = new Media(filePath);
				mediaPlayer = new MediaPlayer(media);
				mediaShow.setMediaPlayer(mediaPlayer);
				mediaShow.setX(400);
	        	mediaShow.setY(350);
				//mediaPlayer.play();	
	}
	}
	 @FXML
	 private void playButton(ActionEvent event) {
		 mediaPlayer.play();
	 }
	 @FXML
	 private void stopButton(ActionEvent event) {
		 mediaPlayer.stop();
	 }
	 @FXML
	 private void chooseFolder(ActionEvent event) {
		 DirectoryChooser directoryChooser = new DirectoryChooser();
		 File dir = directoryChooser.showDialog(null);
		 VBox re = new VBox();
		 scrollPane.setContent(re);
		 re.setSpacing(10);
		 if (dir != null) {
			 listFile = dir.listFiles();
			 lengthListFile = listFile.length; // lay chieu dai cua danh sach bai hat
			 Media media = new Media(listFile[0].toURI().toASCIIString()); // lấy path của file đầu tiên làm mặc định
			 mediaPlayer = new MediaPlayer(media);
			 mediaShow.setMediaPlayer(mediaPlayer);
			 for(File file: listFile) {
				 Button button = new Button(); //tạo nút mới
        		 button.setText(file.getName()); // đặt tên nút
        		// button.setId(file.getPath()); // đặt ID cho nút, lấy id bằng path luôn
        		 button.setId(Integer.toString(i));
        		 button.setOnAction(click->{   // tạo một event trong nút
         		 mediaPlayer.stop(); // dừng cái đang phát hiện tại
         		 mediaPlayer = new MediaPlayer(new Media(file.toURI().toASCIIString())); //đổi thành media mới
      			 mediaPlayer.play();
      			 number = button.getId();// lay id cua button 
      			 numberInt = Integer.parseInt(number);
         		 });
        		 re.getChildren().add(button); // tạo nút xong xuôi thì thêm vào VBOX -> quay lại dòng 90
        		 i = i+1;
			 }
         	
         } else {
             System.out.println("nothing");
         }
     }
	 @FXML
	 private void showSong(ActionEvent event) {
		 System.out.println("you click me");
		 scrollPane.setVisible(true);
	 }
	 @FXML
	 private void playNext(ActionEvent event) {
		if(numberInt !=(lengthListFile-1) ) { // kiem tra id button cung tuc la thu tu cua bai hat co nam o cuoi cung hay khong
		numberInt = numberInt +1; // neu bai khong nam o cuoi thi so thu tu bai hat cong them 1
		mediaPlayer.stop(); // dung bai hat hien tai
		mediaPlayer = new MediaPlayer(new Media(listFile[numberInt].toURI().toString()));
		mediaPlayer.play();// play bai ke tiep
		}else {
			numberInt = 0; // neu thu tu bai hat la cuoi cung thi gan id button bang khong de play bai hat dau tien
			mediaPlayer.stop();
			mediaPlayer = new MediaPlayer(new Media(listFile[numberInt].toURI().toString()));
			mediaPlayer.play();// play bai hat dau tien trong danh sach
			
		}
		 
	 }
	 @FXML
	 private void playPrev(ActionEvent event) {
		 if(numberInt != 0) {// kiem tra id button cung tuc la thu tu bai hat co phai la bai hat dau tien trong danh sach khong 
		 numberInt = numberInt -1;// neu khong phai thi so thu tu bai hat tru di 1
		 mediaPlayer.stop();// dung bai hat hien tai
		 mediaPlayer = new MediaPlayer(new Media(listFile[numberInt].toURI().toString()));
		 mediaPlayer.play();// play bai hat da phat luc truoc
		 }else {
			 numberInt = lengthListFile-1;// neu thu tu bai hat la dau tien thi so thu tu bai hat se bang cheu dai danh sach bai hat tru di 1
			 mediaPlayer.stop();
			 mediaPlayer = new MediaPlayer(new Media(listFile[numberInt].toURI().toString()));
			 mediaPlayer.play();// play bai hat cuoi cung
		 }
		 
		 
	 }
	
		 
	 }
	 		


